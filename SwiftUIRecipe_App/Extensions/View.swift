//
//  View.swift
//  SwiftUIRecipe_App
//
//  Created by MAC on 27/05/21.
//
import SwiftUI

extension View {
    func embedInNavigation() -> some View {
        NavigationView { self }
    }
}
