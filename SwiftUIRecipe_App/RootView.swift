//
//  RootView.swift
//  SwiftUIRecipe_App
//
//  Created by MAC on 27/05/21.
//

import SwiftUI

struct RootView: View {
    var body: some View {
        NavigationView {
            HomeContainerView()
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
